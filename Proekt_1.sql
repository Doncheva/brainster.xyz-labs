-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 05, 2018 at 07:12 PM
-- Server version: 5.7.24-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-3+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Proekt_1`
--

-- --------------------------------------------------------

--
-- Table structure for table `Admin_login`
--

CREATE TABLE `Admin_login` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Admin_login`
--

INSERT INTO `Admin_login` (`id`, `email`, `password`) VALUES
(10, 'simona17mfs@gmail.com', '$2y$10$zQUnYaxTJy9deWUSqN37l.LtdQmFnbifpwWbfLeBqGPMASgZcYct6');

-- --------------------------------------------------------

--
-- Table structure for table `Hiring_companies`
--

CREATE TABLE `Hiring_companies` (
  `id` int(11) NOT NULL,
  `company_name` varchar(20) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Projects`
--

CREATE TABLE `Projects` (
  `id` int(11) NOT NULL,
  `img_path` varchar(250) NOT NULL,
  `title` varchar(50) NOT NULL,
  `subtitle` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `link` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Projects`
--

INSERT INTO `Projects` (`id`, `img_path`, `title`, `subtitle`, `description`, `link`) VALUES
(1, 'https://cdn0.iconfinder.com/data/icons/business-381/500/business-work_13-512.png', 'A Great Idea', 'Convert a Great Idea Into a Reality!', 'How many times have you been in a meeting and someone says to you: That\'s a great idea, you should take the initiative and make it a reality.', 'https://www.forbes.com/sites/glennllopis/2013/04/01/12-things-successfully-convert-a-great-idea-into-a-reality/'),
(4, 'https://cdn.iconscout.com/icon/premium/png-512-thumb/company-corporate-growth-performance-chart-graph-success-rate-1-12407.png', 'What is business growth?', 'The Five Stages of Small Business Growth', 'A company must continue to evolve to stay innovative, relevant and competitive. In almost every individual\'s mind, growth is the definition of success.', 'https://www.forbes.com/sites/forbesagencycouncil/2017/11/20/the-best-kept-secret-of-business-growth/#4eeaa9d1977e'),
(5, 'http://icons-for-free.com/free-icons/png/512/1562684.png', 'Train Your Brain', 'How to Train Your Brain to Think Differently', 'Your brain has the ability to learn and grow as you age, a process called brain plasticity, but for it to do so, you have to train it on a regular basis.', 'https://www.lifehack.org/articles/productivity/8-ways-train-your-brain-learn-faster-and-remember-more.html'),
(6, 'https://cdn0.iconfinder.com/data/icons/cloud-technology/64/touchscreen_icon_technology_touch_screen_innovation_future_digital-512.png', 'Data Technology', 'Turning data into useful and useable Asset.', 'Data Technology is the best and easiest chance to make a lot of money and become a millionaire while you are just an ordinary Internet user.', 'https://www.luckscout.com/what-is-data-technology/'),
(7, 'https://mbtskoudsalg.com/images/marketing-transparent-social-media-5.png', 'Social Media Marketing', 'What do you do as a social media marketer?', 'Social media marketing refers to the process of gaining traffic or attention through social media sites. Social media marketing is the future of small businesses.', 'https://www.wordstream.com/social-media-marketing'),
(8, 'https://spectrocoin.com/vassets/images/public/c15ce65d069438f4a573f9a056c19ee9-exchange_section1.png', 'What is Cryptocurrency and how to use it?', 'Everything You Need To Know', 'A cryptocurrency is a digital or virtual currency designed to work as a medium of exchange. It uses cryptography to secure and verify transactions.', 'https://cointelegraph.com/bitcoin-for-beginners/what-are-cryptocurrencies'),
(9, 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQYCzQCzBbdviY5GY46ImSDpCsriDbgdT_xFW8GT_vkhMC5Zwer', 'Data entry', 'Learn how to do data entry', 'Data entry is the act of entering information into electronic formats by using word processing or data processing software hosted on a computer and it\'s data entry operators who perform these tasks.', 'https://www.thebalancecareers.com/what-is-data-entry-3542483'),
(10, 'https://cdn4.iconfinder.com/data/icons/smart-phone-computer/512/13-512.png', 'Coding', 'Practice coding with fun programming challenges', 'Want to practice coding? Try to solve these programming puzzles (25+ languages supported).', 'https://www.codingame.com/training');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Admin_login`
--
ALTER TABLE `Admin_login`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `Hiring_companies`
--
ALTER TABLE `Hiring_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Projects`
--
ALTER TABLE `Projects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Admin_login`
--
ALTER TABLE `Admin_login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `Hiring_companies`
--
ALTER TABLE `Hiring_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `Projects`
--
ALTER TABLE `Projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
