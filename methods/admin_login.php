<?php

require "../../config.php";

$emailErr = $passErr = "";
$email = $login_password = "";

if ($_POST) {

    if (empty (trim($_POST["email"]))) {
    $emailErr = "<i class='fas fa-exclamation-circle'></i> Внесете адреса за е-пошта!";
    } else {
    $email = trim($_POST["email"]);
    }
    if (empty (trim($_POST["login_password"]))) {
    $passErr = "<i class='fas fa-exclamation-circle'></i> Внесeте лозинка!";
    } else {
    $login_password = trim($_POST["login_password"]);
    }

    if(empty($emailErr) && empty($passErr)) {
        $sql = "SELECT * FROM Admin_login WHERE email = :email";

        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(':email', $param_email, PDO::PARAM_STR);
            $param_email = trim($_POST["email"]);

            if ($stmt->execute()) {
                if ($stmt->rowCount() == 1) {
                    if ($row = $stmt->fetch()) {
                        $hashed_password = $row['password'];

                        if (password_verify($login_password, $hashed_password)) {
                            session_start();
                            $_SESSION['email'] = $email;
                            header('location:be_useful.php');
                        }
                        else {
                            $passErr = "<i class='fas fa-exclamation-circle'></i> Внесовте погрешна лозинка!";
                        }
                    }
                }
                else {
                    $emailErr = "<i class='fas fa-exclamation-circle'></i> Внесовте погрешна адреса за е-пошта!";
                }
            }
            else {
                echo "Обидете се повторно!";
            }
        }
        unset($stmt);
    }
    unset($pdo);
}



