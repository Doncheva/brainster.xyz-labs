<?php
require "../config.php";


if($_POST['method'] == 'add') {
	$img_path = $_POST['img_path'];
	$title = $_POST['title'];
	$subtitle = $_POST['subtitle'];
	$description = $_POST['description'];
	$link = $_POST['link'];


	$sql = "INSERT INTO Projects(img_path, title, subtitle, description, link) VALUES (?, ?, ?, ?, ?)";

	$stmt = $pdo->prepare($sql);

	
    $stmt->bindParam(1, $img_path);
    $stmt->bindParam(2, $title);
	$stmt->bindParam(3, $subtitle);
	$stmt->bindParam(4, $description);
	$stmt->bindParam(5, $link);

	$stmt->execute();

	if($stmt->rowCount() > 0) {
		header("Location: ../view/admin/be_useful.php?status=success&msg=".urlencode("Success"));
		die();
	} else {
		header("Location: ../view/admin/be_useful.php?status=error");
		die();
	}

}

else if($_GET['method'] == 'delete') {

    $id = $_GET['id'];

	redirectIfNotSelected($id);

	$sql = "DELETE FROM Projects WHERE id = :id";
	$stmt = $pdo->prepare($sql);
	$stmt->execute(['id' => $id]);

	if($stmt->rowCount() > 0) {
		header("Location: ../view/admin/project_action.php?status=success&msg=".urlencode("Success"));
		die();
	} else {
		header("Location: ../view/admin/project_action.php?status=error");
		die();
	}

}

else if($_GET['method'] == 'update') {

    $id= $_GET['id'];

	redirectIfNotSelected($id);

    header("Location: ../view/admin/edit_project.php?id=".$id);
	die();
}

else if($_POST['method'] == 'store') {
    $img_path = $_POST['img_path'];
	$title = $_POST['title'];
	$subtitle = $_POST['subtitle'];
	$description = $_POST['description'];
	$link = $_POST['link'];
    $id = $_POST['id'];

	$sql = "UPDATE Projects SET img_path=:img_path, title=:title, subtitle=:subtitle, description=:description, link=:link WHERE id=:id LIMIT 1";
	$stmt = $pdo->prepare($sql);

	if($stmt->execute(['img_path' => $img_path, 'title'=>$title, 'subtitle'=>$subtitle, 'description'=>$description, 'link' => $link, 'id'=>$id])) {
		header("Location: ../view/admin/be_useful.php?status=success&msg=".urlencode("Success"));
		die();
	} else {
		header("Location: ../view/admin/edit_project.php?status=error");
		die();
	}
}

function redirectIfNotSelected($id) {
	if($id == -1) {
		header("Location: ../view/admin/project_action.php?status=error");
		die();
	}
}