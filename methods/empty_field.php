<?php
require "../config.php";

$companyNameErr = $emailErr = $telErr = "";
$company_name = $email = $tel = "";

if ($_POST) {
    if (empty($_POST["company_name"])) {
        $companyNameErr = "<i class='fas fa-exclamation-circle'></i> Внесете име на фирма!";
    } else {
        $company_name = $_POST["company_name"];
    }

    if (empty($_POST["email"])) {
        $emailErr = "<i class='fas fa-exclamation-circle'></i> Внесете адреса за е-пошта!";
    } elseif (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $emailErr = "<i class='fas fa-exclamation-circle'></i> Внесете валидна адреса за е-пошта!";
    } else {
        $email = $_POST["email"];
    }

    if ((empty($_POST["tel"]))) {
        $telErr = "<i class='fas fa-exclamation-circle'></i> Внесете телефон за контакт!";
    } elseif (!(is_numeric($_POST['tel']))) {
        $telErr = "<i class='fas fa-exclamation-circle'></i> Телефонот за контакт може да содржи само бројки!";
    } else {
        $tel = $_POST["tel"];
    }

    if (empty($CompanyNameErr) && empty($emailErr) && empty($telErr)) {

        $sql = "INSERT INTO Hiring_companies(company_name, mail, phone)
    VALUES ('$company_name', '$email', '$tel')";

        $stmt = $pdo->query($sql);

    }
}
