
<?php  

require "../methods/empty_field.php";
include "_partials/_head.php";
include_once "_partials/_modal.php";

?>

<body class="hire-body">
	<div class="container-fluid gif-hire">
		<div class="row main_content-hire  background_gif-hire">

			<!--form-->
			<div class="col-sm-4 col-sm-offset-4">
				<div class="form_style-hire form_style">
                    <div class="text-center legend"><b>Внесете ги вашите податоци</b></div>
	                    <form class="form-group" method="POST" action="hire.php">
	                    	<span class="error brainster_color"><?= $companyNameErr ?></span>
	                        <input type="text" name="company_name" class="form-control" placeholder="Име на фирма" value="<?= $company_name ?>">
	                        <span class="error brainster_color" style="color: #FCD232;"><?= $emailErr ?></span>
	                        <input type="email" name="email" class="form-control" placeholder="Адреса за е-пошта" value="<?= $email ?>">
	                        <span class="error brainster_color"><?= $telErr ?></span>
	                        <input type="text" name="tel" class="form-control" placeholder="Телефон за контакт" value="<?= $tel ?>">
                            <a class=" modal-opener" href="" data-toggle="modal" data-target="#myModal"><button type="submit" class="form-control text-center btn" name="submit">Испрати</button></a>
	                    </form>
            	</div>
			</div>
		</div>
		
		<div class="row">
			<!--footer-->
			<div class="brainster_background footer-hire footer">
				<div class="logo-hire">
					<img id="brainster_logo" src="../public/img/Brainster-Logo-03.png">
				</div>	
				<a href="index.php" type="button" name="back" class="btn_back-hire" >Врати се назад</a>
			</div>
		</div>
	</div>

</body>
