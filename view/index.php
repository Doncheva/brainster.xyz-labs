<?php
require "../config.php";

include_once "_partials/_head.php";

?>


<body class="index-body">

	<!--========================================================-->
	<!-- NAVBAR -->
	<?php
		include_once "_partials/_navbar.php";
	?>

	<!--========================================================-->
	<!-- GIT BACKGROUND -->

	<div class="container-fluid gif-index">
		<div class="row background_gif-index">
			<div class = "text-center white">
				<h1 class="index-h1">Brainster.xyz Labs</h1>
				<h4 class="index-h4">Проекти на студентите на академиите за програмирање и маркетинг на Brainster</h4>
			</div>
		</div>
	</div>


    <!--========================================================-->
    <!-- CARDS -->
    <?php
		include_once "_partials/_card-section.php";
	?> 


	<!--========================================================-->
	<!-- FOOTER -->
    <?php
		include_once "_partials/_footer.php";
	?> 


</body>