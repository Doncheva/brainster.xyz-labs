<?php
    include_once "../_partials/_head.php";
?>

<body>

<!-- task cards-->
<div class="container-fluid body">
    <div class="row main_content-be-useful text-center text-uppercase custom_container">
        <div class="card_wrap">
            <div class="col-xs-12 col-sm-4 text-center ">
                <a class="task" href="project_action.php">
                    <div class="card card-useful">
                        Направи промена на постоечка картичка
                    </div>
                </a>

                <a class="task" href="new_project.php">
                    <div class="card card-useful">
                        Внеси нова картичка
                    </div>
                </a>

                <a class="task" href="company_view.php">
                    <div class="card card-useful">
                        Фирми кои сакаат да вработат студенти
                    </div>
                </a>
            </div>

        </div>
        <div class="hidden-xs img">
            <img src="../../public/img/bulb.png">
        </div>

    </div>



    <div class="row quote">
        <!-- quote -->
        <div class="blockquote_container white">
            <blockquote class="blockquote-reverse">
                <p>Knowledge is of no value <br> unless you put it into practice.</p>
                <footer>Anton Chekhov</footer>
            </blockquote>
        </div>
    </div>


</div>

</body>