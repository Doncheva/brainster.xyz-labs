<?php

require "../../config.php";
include_once "../_partials/_head.php";


	$sql = "SELECT * FROM Projects";

	$stmt = $pdo->query($sql);
?>


<body>

<div class="container-fluid body">
    <div class="row main_content min-body-height">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
            <div class="form_style form_style-hire">
                <div class="text-center legend">
                    <form method="POST" action="../../methods/project.php">
                        <input type="hidden" name="method" value="add"/>
                        <input required class="form-control"  type="text" name="title" placeholder="Внесете наслов"/>
                        <input required class="form-control" type="url" name="img_path" placeholder="Внесете URL од слика"/>
                        <input required class="form-control" type="text" name="subtitle" placeholder="Внесете поднаслов"/>
                        <textarea required rows="3" class="form-control" name="description" placeholder="Внесете краток опис"></textarea>
                        <input required class="form-control" type="url" name="link" placeholder="Внесете линк"/>
                        <button class="form-control text-center btn" type="submit">Зачувај</button>
                    </form>
                </div>

            </div>

        </div>
    </div>
</div>

    <?php

         include_once "../_partials/_admin_footer.php";

    ?>

</body>