<?php

require "../../config.php";
include_once "../_partials/_head.php";


$sql = "SELECT * FROM Projects WHERE id=? LIMIT 1";

$stmt = $pdo->prepare($sql);

$stmt->execute([$_GET['id']]);

if($stmt->rowCount() !== 1) {
    header("Location: project_action.php");
    die();
}

$row = $stmt->fetch();
?>


<body>

<div class="container-fluid body">
    <div class="row main_content min-body-height">
        <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4">
            <div class="form_style form_style-hire">
                <div class="text-center legend">
                    <form method="POST" action="../../methods/project.php">
                        <input type="hidden" name="method" value="store"/>
                        <input type="hidden" name="id" value="<?= $_GET['id'] ?>"/>
                        <input class="form-control" type="url" name="img_path" value="<?= $row['img_path'] ?>"/>
                        <input class="form-control"  type="text" name="title" value="<?= $row['title'] ?>"/>
                        <input class="form-control" type="text" name="subtitle" value="<?= $row['subtitle'] ?>"/>
                        <textarea maxlength="200" class="form-control" name="description"><?= $row['description'] ?></textarea>
                        <input class="form-control" type="text" name="link" value="<?= $row['link'] ?>"/>
                        <button class="form-control text-center btn" type="submit" value="store">Зачувај ги промените</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php

        include_once "../_partials/_admin_footer.php";

    ?>

</body>
