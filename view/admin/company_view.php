<?php

require "../../config.php";
include_once "../_partials/_head.php";

$sql = "SELECT * FROM Hiring_companies";

$stmt = $pdo->query($sql);
?>


<body>

<div class="container-fluid body">
    <div class="row main_content white">
        <div class="col-xs-10 col-xs-offset-1" >
            <div class="table-responsive" >

                <?php

                try {
                    if($stmt->rowCount() > 0) {

                ?>

                <table class='table text-center' id='header-fixed' style="height: 89vh; overflow-y: scroll;">
                    <thead>
                        <tr class='active' style='color: #353535;'>
                            <th class='text-center'>Р.Број</th>
                            <th class='text-center'>Име на фирма</th>
                            <th class='text-center'>Адреса за е-пошта</th>
                            <th class='text-center'>Телефон за контакт</th>
                        </tr>
                    </thead>
                    <tbody>

                <?php

                    while($row = $stmt->fetch()) {

                ?>
                        <tr>
                            <td><?= $row['id'] ?></td>
                            <td><?= $row['company_name'] ?></td>
                            <td><?= $row['mail'] ?></td>
                            <td><?= $row['phone'] ?></td>
                        </tr>
                       <?php } ?>
                    </tbody>
                </table>

                <?php
                        unset($stmt);
                    } else{
                        echo "<div class='main_content-hire' id='empty_db'>
                                <h2 class='text-center' style='margin: 0'>Базата е празна!<h3>
                               </div>";
                    }
                } catch(PDOException $e){
                    die("ERROR: Error in execute $sql. " . $e->getMessage());
                }
                unset($pdo);

                ?>

            </div>
        </div>
    </div>
</div>

    <?php

         include_once "../_partials/_admin_footer.php";

    ?>

</body>