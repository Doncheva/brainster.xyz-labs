<?php

require "../../config.php";
include_once "../_partials/_head.php";

$sql = "SELECT * FROM Projects";

$stmt = $pdo->query($sql);
$stmt->execute();
$cards = $stmt->fetchAll();
?>

<body class="index-body">

<div class="container margin min-body-height">
    <div class="row custom_container" style="padding-bottom: 5%">
        <?php

        if ($stmt->rowCount() > 0) {
            foreach ($cards as $single_card ) { 

        ?>

        <div class='col-xs-12 col-sm-4 text-center slide-up link' >
            <a href="<?= $single_card['link'] ?>" target="_blank">
                <div class='card' style='height: 300px; margin-top: 50px; '>
                    <img src="<?= $single_card['img_path'] ?>">
                    <div class='card-body'>
                        <small><h3 class='card-title'><?= $single_card['title'] ?></h3></small>
                        <small><p class='h4 card-subtitle'><?= $single_card['subtitle'] ?></p></small>
                        <p class='card-text'><?= $single_card['description'] ?></p>
                    </div>
                <div class='overlay'></div>
                </div>
            </a>

            <?php

                $id = $single_card["id"];
                $method_del = "delete";
                $method_update = "update";


            echo "<a href = '../../methods/project.php?id=$id&method=$method_update'>
                <button class='btn_change btn' name='method' value='update'><i class='fas fa-edit'></i> Уреди </button>
            </a >";
            echo "<a href = '../../methods/project.php?id=$id&method=$method_del'>
                <button class='btn_change btn' name='method' value='delete'><i class='fas fa-trash-alt'></i> Избриши </button>
            </a >" ?>
        </div>

        <?php
            }
        }
        ?>
    </div>
</div>

    <?php

        include_once "../_partials/_admin_footer.php";

    ?>

</body>