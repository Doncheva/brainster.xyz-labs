<?php

require "../../methods/admin_login.php";
include "../_partials/_head.php";

?>


<body>
	<!-- form -->
	<div class="container-fluid body-admin-login body">
		<div class="row"><img id="bulb-img" src="../../public/img/bulb.png">
			<div class="col-sm-4 col-sm-offset-4">

				<div class="form_style ">
                	<div class="text-center legend"></div>
	                    <form method="POST" action="admin_index.php">
                            <div class="form-group brainster_color <?= (!empty($emailErr)) ? 'has-error' : ''; ?>">
                                <span class="error brainster_color"><?= $emailErr;?></span>
                                <input type="email" name="email" class="form-control" placeholder="Адреса за е-пошта" value="<?= $email; ?>">
                            </div>
                            <div class="form-group brainster_color <?= (!empty($passErr)) ? 'has-error' : ''; ?>">
                                <span class="error brainster_color"><?= $passErr;?></span>
                                <input type="password" name="login_password" class="form-control" placeholder="Лозинка">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="form-control text-center btn" name="submit"> Get Shit Done</button>
                            </div>
	                    </form>
            	</div>
			</div>
		</div>
	</div>

	<!-- quote -->
	<div class="blockquote_container white">
	  	<blockquote class="blockquote-reverse">
	    	<p>It has been my observation that most people get ahead <br>during the time that others waste.</p>
	    <footer>Henry Ford</footer>
	  	</blockquote>
	</div>

</body>