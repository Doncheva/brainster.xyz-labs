<div class="container-fluid footer text-center">
    <div class="row">
        <h5>
            <small>Made with  &nbsp;<i class="fas fa-heart"></i>&nbsp;  by</small>
            <img src="../public/img/Brainster-Logo-03.png">
            <a href="https://www.facebook.com/brainster.co">
                <small class="small">- Say Hi! - </small class="small">
            </a>
            <small> Terms</small>
        </h5>
    </div>
</div>