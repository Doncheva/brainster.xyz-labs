<?php 

$sql = "SELECT * FROM Projects";

$stmt = $pdo->query($sql);
$stmt->execute();
$cards = $stmt->fetchAll();
?>



<div class="container margin">
    <div class="row custom_container">
        <?php

        if ($stmt->rowCount() > 0) {
            foreach ($cards as $single_card ) { 

        ?>

        <div class='col-xs-12 col-sm-4 text-center slide-up link' >
            <a href="<?= $single_card['link'] ?>" target="_blank">
                <div class='card card-index' style='height: 300px; margin-top: 20px; '>
                    <img src="<?= $single_card['img_path'] ?>">
                    <div class='card-body'>
                        <small><h3 class='card-title'><?= $single_card['title'] ?></h3></small>
                        <small><p class='h4 card-subtitle'><?= $single_card['subtitle'] ?></p></small>
                        <p class='card-text'><?= $single_card['description'] ?></p>
                    </div>
                <div class='overlay'></div>
                </div>
            </a>
        </div>

        <?php
            }
        }
        ?>
    </div>
</div>





