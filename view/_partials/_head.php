<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Brainster.xyz Labs</title>
	<meta name="description" content="Проекти на студентите на академиите за програмирање и маркетинг на Brainster">
	<meta name="author" content="Simona Doncheva">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="//fonts.googleapis.com/css?family=Ubuntu:400,500,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="../public/css/index.css">
	<link rel="stylesheet" type="text/css" href="../public/css/hire.css">
	<link rel="stylesheet" type="text/css" href="../public/css/general.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/index.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/hire.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/general.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/be_useful.css">
	<link rel="stylesheet" type="text/css" href="../../public/css/amdin_index.css">
	<link rel="stylesheet" 
		href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 
		integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" 
		crossorigin="anonymous">
	<link rel="stylesheet" 
		href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" 
		integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" 
		crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>
