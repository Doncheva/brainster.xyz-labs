<div class="container-fluid brainster_background navbar">
	<div class="content">
		<!--logo-->
		<div class="logo">
			<a class="img-responsive" href="https://brainster.co/"><img id="brainster_logo" src="../public/img/Brainster-Logo-03.png"></a>
		</div>

		<!--collapse button-->	
		<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mainNavBar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
			
	<!--menu items-->
	<div class="collapse navbar-collapse navbar-right text-center" id="mainNavBar">
		<ul>
			<li class="li"><a href="http://codepreneurs.co/">Академија за <br> Програмирање</a></li>
			<li class="li"><a href="http://marketpreneurs.co/akademija-za-marketing-programa/">Академија за <br> Маркетинг</a></li>
			<li class="li yp"><a href="https://blog.brainster.co/">Блог</a></li>
			<li class="li nplast"><a href="hire.php">Вработи наши <br> студенти</a></li>
		</ul>
	</div>
</div>