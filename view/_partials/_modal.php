<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content custom-modal">
            <div class="modal-header">
                <button type="button" class="close close-style" data-dismiss="modal">&times;</button>
                <h3>Почитувани,</h3>
            </div>
            <div class="modal-body ">
                <h5>Вашите податоци успешно се зачувани во нашата база.</h5>
                <h5>Ви благодариме на довербата!</h5>
                <br>
            </div>
        </div>
    </div>
</div>