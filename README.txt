Brainster.labs is a project made for Brainster.co.
The file named "Documentation.pdf" is the requirements.
There is a file named "Proekt_1.sql" where you can find all the information to set up the database for the project.
In this project I used: HTML, CSS, Bootstrap, SQL, PHP. (It is not a Laravel project).
After downloading the project, you can access it from your localhost address -> path -> /view/index.php
Also there is admin panel which can be accessed from your localhost address -> path -> /view/admin/admin_index.php
In the "Admin Acces.txt" file you will find the email and password required for admin login. 
